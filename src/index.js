import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import ServiceWorkerNotify from './Service-Worker-Notify';

let swn = new ServiceWorkerNotify();

ReactDOM.render(
  <React.StrictMode>
    <App notify={swn} />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register(swn);
